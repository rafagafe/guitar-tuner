
/* ------------------------------------------------------------------------ */
/** @file  switch.h
  * @brief Interface Switch module.
  * @date   20/09/2015
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _SWITCH_
#define	_SWITCH_

/** @defgroup switch Switch Driver
  * This module implements interrupt driven processes to wait for switch
  * actions in low power mode. WDT is used as debounce timer. @{  */

/** Configure hardware and driver state variav¡bles. */
void switch_config( void );

/** Wait until the switch is pushed in LPM0. */
void switch_waitUntilPush( void );

/** Wait until the switch is pushed in LPM0. */
void switch_shutDwonUntilPush( void );

/** @ } */

#endif	/* _SWITCH_ */

