
/* ------------------------------------------------------------------------ */
/** @file  main.c
  * @brief Entry point of application.
  * @date   20/09/2015
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ 
                    _ _               _                         
         __ _ _   _(_) |_ __ _ _ __  | |_ _   _ _ __   ___ _ __ 
        / _` | | | | | __/ _` | '__| | __| | | | '_ \ / _ \ '__|
       | (_| | |_| | | || (_| | |    | |_| |_| | | | |  __/ |   
        \__, |\__,_|_|\__\__,_|_|     \__|\__,_|_| |_|\___|_|   
        |___/  
 
   ------------------------------------------------------------------------ */

#include <msp430.h>
#include "uart.h"
#include "stroboscope.h"
#include "switch.h"

/** Welcome message. */
const char * _welcome = "\x1B[2J"
"\t             _ _               _                         \n\r"
"\t  __ _ _   _(_) |_ __ _ _ __  | |_ _   _ _ __   ___ _ __ \n\r"
"\t / _` | | | | | __/ _` | '__| | __| | | | '_ \\ / _ \\ '__|\n\r"
"\t| (_| | |_| | | || (_| | |    | |_| |_| | | | |  __/ |   \n\r"
"\t \\__, |\\__,_|_|\\__\\__,_|_|     \\__|\\__,_|_| |_|\\___|_|   \n\r"
"\t |___/  \n\r"
"\n\rPress the switch to start.\n\r";


/** Messages to send to terminal. */
char *msg[] = {
    "\n\rGuitar tuner is off.\n\r\n\r",
    "String 1, E, Mi.\n\r",
    "String 2, B, Si.\n\r",
    "String 3, G, Sol.\n\r",
    "String 4, D, Re.\n\r",
    "String 5, A, La.\n\r",
    "String 6, E, Mi.\n\r"  
};

/** Entry point. */
int main( void ) {
    
    /* Basic mcu config: */
    WDTCTL = WDTPW | WDTHOLD;
    BCSCTL1 = CALBC1_1MHZ; 
    DCOCTL = CALDCO_1MHZ;  
    __eint();
        
    /* Modules config: */
    uart_config();
    stroboscope_config();
    switch_config();   
    
    uart_waitToSend( (char*)_welcome );

    /* Main loop: */
    while( 1 ) {   
        
        switch_shutDwonUntilPush();
        
        /* For each string: */
        for( int i = 6; i >= 0; i-- ) {                        
            uart_waitToSend( msg[i] );
            stroboscope_string( i ); 
            if ( i ) switch_waitUntilPush();            
        }        
    }     
    
    return 0;
    
}

/* ------------------------------------------------------------------------ */

