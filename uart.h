
/* ------------------------------------------------------------------------ */
/** @file  uart.h
  * @brief Interface Serial Port Driver module.
  * @date   20/09/2015
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _UART_
#define	_UART_

/** @defgroup uart Serial Port Driver
  * This module implements interrupt driven processes to tranfer text by 
  * serial port. @{  */

/** Configure hardware and driver state variav¡bles. */
void uart_config( void );

/** Try to launch an interrupt driven process that sends a string by uart.
  * While the launching is not possible it waits in LPM0.
  * @param string: Pointer to string source. */
void uart_waitToSend( char *string );

/** @ } */

#endif	/* _UART_ */

