NAME            = guitar-tuner
OBJECTS         = main.o stroboscope.o switch.o uart.o
SRCS 						= main.c stroboscope.c switch.c uart.c
CPU             = msp430g2553
CFLAGS          = -mmcu=${CPU} -Wall -g -std=c99
CC							= msp430-gcc
DRIVER          = rf2500

.PHONY: build all FORCE clean debug dist sim terminal help 
	
build: ${NAME}.elf

all: clean ${NAME}.elf ${NAME}.a43 ${NAME}.lst

clean:
	rm -f ${NAME}.elf ${NAME}.a43 ${NAME}.lst ${OBJECTS} script.gdb dependencies.mk

sim: ${NAME}.elf script.gdb
	mspdebug sim "prog ${NAME}.elf" gdb &
	msp430-gdb -x script.gdb "${NAME}.elf"
	rm -f script.gdb core	
	
debug: ${NAME}.elf script.gdb
	mspdebug ${DRIVER} gdb & msp430-gdb ${NAME}.elf -x script.gdb
	rm -f script.gdb 	

run: ${NAME}.elf
	mspdebug ${DRIVER} "prog ${NAME}.elf"
	
terminal: run	
	minicom -D /dev/ttyACM0 -b 9600

dist:
	tar czf dist.tgz *.c *.h makefile

help:
	@echo "Targets:"
	@echo "\t-build (default)"
	@echo "\t-all"
	@echo "\t-clean"
	@echo "\t-debug"
	@echo "\t-run"
	@echo "\t-terminal"
	@echo "\t-dist"
	@echo "\t-help"
	
${NAME}.elf: ${OBJECTS}
	${CC} -mmcu=${CPU} -o $@ ${OBJECTS}

${NAME}.a43: ${NAME}.elf
	msp430-objcopy -O ihex $^ $@

${NAME}.lst: ${NAME}.elf
	msp430-objdump -dSt $^ >$@

script.gdb:
	echo "target remote localhost:2000\nerase all\nload\nbreak main\ncontinue" > script.gdb

dependencies.mk: ${SRCS}
	${CC} ${CFLAGS} -MM $^ > dependencies.mk

include dependencies.mk


