
/* ------------------------------------------------------------------------ */
/** @file  stroboscope.h
  * @brief Interface Stroboscoper module.
  * @date   20/09/2015
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#ifndef _STROBOSCOPE_
#define	_STROBOSCOPE_

/** @defgroup stroboscope Stroboscope Driver
  * This module implements methods to configure a timer as stroboscope. @{  */

/** Configure hardware and driver state variav¡bles. */
void stroboscope_config( void );
  
/** Set stroboscope to tune a determined string.
  * @param number: String index, 0 to turn off. */
void stroboscope_string( unsigned number );

/** @ } */
#endif	/* _STROBOSCOPE_ */

