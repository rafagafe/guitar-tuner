
/* ------------------------------------------------------------------------ */
/** @file uart.c
  * @brief This module implements interrupt driven processes to wait for 
  * switch actions in low power mode. 
  * @date   10/02/2015.
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#include <msp430.h>
#include <legacymsp430.h>
#include <stdbool.h>
#include "wating.h"

/** Bit in GPIO port of swith. */
#define SWITCH              BIT3

/** Bit in GPIO port of red LED. */
#define RED                 BIT0

/** Indicates that debounce timer is running. */
static volatile bool _debounceTime = false;


/* Configure hardware and driver state variav¡bles. */
void switch_config( void ) {
    P1DIR &= ~SWITCH; 
    P1REN |= SWITCH;  
    P1OUT |= SWITCH;  
    P1IES |= SWITCH;
    P1IFG &= ~SWITCH;       
    P1DIR |= RED;     
}

/** Wait until the switch is pushed in low power mode.
  * @param lpm: Low Power Mode Bits. */
static void _waitPush( unsigned lpm ) {  
    
    P1OUT &= ~RED;
    
    /* Wait until released: */
    P1IES &= ~SWITCH;
    wait_until( P1IN & SWITCH, LPM0_bits ) ;
    
    /* Start debounce timer: */
    P1IE &= ~SWITCH; 
    _debounceTime = true;    
    WDTCTL = WDT_ADLY_16;
    IFG1 &= ~WDTIFG;             
    IE1 |= WDTIE;  
    
    /* Wait for debounce time: */
    wait_while( _debounceTime, LPM0_bits );
              
    /* Wait until pressed: */  
    P1IE |= SWITCH;     
    P1IES |= SWITCH;  
    wait_while( P1IN & SWITCH, lpm ) ;
    
    P1OUT |= RED;                 
}

/* Wait until the switch is pushed in LPM0. */
void switch_waitUntilPush( void ) { _waitPush( LPM0_bits ); }

/* Wait until the switch is pushed in LPM4. */
void switch_shutDwonUntilPush( void ) { _waitPush( LPM4_bits ); }


interrupt (PORT1_VECTOR) PORT1_ISR(void) {  
    P1IFG &= ~SWITCH;
    LPM4_EXIT; 
}

interrupt (WDT_VECTOR) WDT_SIR(void) {
    IE1 &= ~WDTIE;                   
    IFG1 &= ~WDTIFG;                 
    WDTCTL = WDTPW + WDTHOLD;        
    _debounceTime = false;
    LPM4_EXIT;
}

/* ------------------------------------------------------------------------ */

