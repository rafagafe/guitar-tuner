
/* ------------------------------------------------------------------------ */
/** @file uart.c
  * @brief This module implements interrupt driven processes to tranfer 
  * text by serial port.
  * @date   10/02/2015.
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#include <msp430.h>
#include <legacymsp430.h>
#include <intrinsics.h>
#include "wating.h"

/** Pointer to sending string. If null no process is working. */
static volatile char* _txString;

/* Configure hardware and driver state variav¡bles. */
void uart_config( void ) {    
    UCA0CTL1 |= UCSWRST;
    _txString = 0;    
    P1SEL  |= BIT1 | BIT2;  // P1.1 <-> RXD, P1.2 <-> TXD
    P1SEL2 |= BIT1 | BIT2;  // P1.1 <-> RXD, P1.2 <-> TXD
    UCA0CTL1 |= UCSSEL_2;   // Use SMCLK
    UCA0BR0 = 104;          // Baudrate to 9600 with 1MHz clock
    UCA0BR1 = 0;   
    UCA0MCTL = UCBRS0;      // Modulation UCBRSx = 1
    UCA0CTL1 &= ~UCSWRST;   // Initialize USCI state machine
    IFG2 &= ~UCA0TXIFG;
    IE2 |= UCA0TXIE;        // Enable USCI_A0 RX and TX interrupt        
}

/* Try to launch an interrupt driven process that sends a string by uart. */
void  uart_waitToSend( char *string ) { 
    wait_while( _txString, CPUOFF );
    _txString = string;
    IFG2 |= UCA0TXIFG;
    IE2 |= UCA0TXIE;      
}

/** USCIA0 TX ISR. */
interrupt(USCIAB0TX_VECTOR) USCI_TX_ISR( void ) {
    if ( _txString && *_txString )
        UCA0TXBUF = *_txString++;
    else {
        IE2 &= ~UCA0TXIE; 
        _txString = 0;  
        LPM4_EXIT;
    }
}

/* ------------------------------------------------------------------------ */
