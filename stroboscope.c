
/* ------------------------------------------------------------------------ */
/** @file stroboscope.c
  * @brief This implements methods to configure a timer as stroboscope.
  * @date   10/02/2015.
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#include <msp430.h>

#define _STRING_1  3033 // (E): 329,63 Hz
#define _STRING_2  4049 // (B): 246,94 Hz 
#define _STRING_3  5101 // (G): 196,00 Hz
#define _STRING_4  6810 // (D): 146,83 Hz 
#define _STRING_5  9090 // (A): 110,00 Hz 
#define _STRING_6 12133 // (E):  82,41 Hz 
#define _DUTY       0.05

/** Periods of string in raw counter. */
static unsigned _periods[] = { 
    _STRING_1, _STRING_2, _STRING_3, _STRING_4, _STRING_5, _STRING_6
};

/** Duty cycle of string in raw counter. */
static unsigned _duty[] = { 
    (unsigned)(_STRING_1*_DUTY), (unsigned)(_STRING_2*_DUTY),
    (unsigned)(_STRING_3*_DUTY), (unsigned)(_STRING_4*_DUTY),
    (unsigned)(_STRING_5*_DUTY), (unsigned)(_STRING_6*_DUTY)
};

/* Configure hardware and driver state variav¡bles. */
void stroboscope_config( void ) {
    P1DIR |= BIT6;
    P1SEL |= BIT6;
    TA0CTL = 0;
}
  
/* Set stroboscope to tune a determined string. */
void stroboscope_string( unsigned number ) {
    if ( number > 6 ) number = 0;
    if ( number ) {
        P1DIR |= BIT6;
        number--;
        TA0CCR0   = _periods[number];
        TA0CCR1   = _duty[number];
        TA0CCTL1 |= OUTMOD_7;
        TA0CTL   |= TASSEL_2 + MC_1;
    }
    else {
        P1DIR &= ~BIT6;
        TA0CTL = 0;        
    }
}

/* ------------------------------------------------------------------------ */
